# Introduction

`runescape3-api` is an open-source wrapper, written in [Python](https://www.python.org/), that allows interaction with the various APIs available for the popular MMORPG [RuneScape3](http://www.runescape.com).

### Contact
`ballioli1@gmail.com`

### Installation

```
pip install runescape3-api
```

### Usage

The implementation offers functionality to interact with the following:

* [Bestiary](https://ballioli1.gitbook.io/runescape3-api/docs/bestiary)
* [Hiscores](https://ballioli1.gitbook.io/runescape3-api/docs/hiscores)
* [RuneMetrics](https://ballioli1.gitbook.io/runescape3-api/docs/runemetrics)