from .hiscores import Hiscores
from .bestiary import Bestiary
from .runemetrics import Runemetrics
